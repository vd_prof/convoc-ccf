# convoc-ccf

Ceci est la classe LaTeX que j'utilise pour créer mes convocations pour les épreuves de CCF de mes BTS.

On peut générer facilement une TODO liste, une liste d'émargement, une liste des groupes at bien sûr, les convocations.

Quelques précisions:
 * La TODO liste est à faire vous-même. C'est un itemize un peu customisé.
 * La liste d'émargement: Elle présente 2 cases signature par étudiant: une pour la réception de la convocation, l'autre pour le passage effectif du CCF.
 * La liste administrative: Elle rassemble les groupes en précisant les horiares, les salles, les noms ainsi que les éventuels 1/3 temps et temps de loge.
 * Les convocations: Une page par étudiant. Elles sont naturellement triée par ordre alphabétique.

 Pour plus d'aide, vous pouvez consulter la documentation et l'exemple.
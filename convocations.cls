%%
%% Ceci définie ma classe pour éditer mes convocations pour les CCF de mes BTS.
%%
%% V. Deveaux (c) 2024
%% Pour toute question, vincent.deveaux@ac-grenoble.fr
%%

%% DISCLAIMER:
%%
%% Cette classe est fournie sans prétention aucune. Elle n'est pas parfaite.
%% Je ne garantie pas qu'il n'y ait aucun bug ou problème de compatibilité.
%% Je décline toute responsabilité en cas de perte de données ou autre, 
%% supposé être causé par l'utilisation de cette classe.
%%

%% LICENCE:
%%
%% Je place ce document, ainsi que sa documentation et le document d'exemple
%% sous licence WTFPL.
%%
%% Voir www.wtfpl.net pour plus de précision (ou la page wikipedia de la WTFPL)

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{convocations}[2024/05/08, version v0.99]

\LoadClass[a4paper,11pt]{lettre}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage[french]{babel}
\RequirePackage{amssymb}  % pour les cases de la todo liste
\RequirePackage{graphicx} % pour l'insertion du logo
\RequirePackage{datatool} % pour la gestion des bases de données
\RequirePackage{supertabular} % pour la liste d'émargement sur plusieurs pages
\RequirePackage{multirow} % pour la liste par groupe
\RequirePackage[table]{xcolor} % pour la liste par groupe
\RequirePackage{xstring} % pour quelques manipulations de chaines
\RequirePackage{datetime} % pour la gestion des dates


\pagestyle{empty}

\newcounter{emargecnt} % Pour compter les pages de la liste d'émargement


%%
%%    Les longueurs
%%
\newlength{\largeurLogo} % largeur du logo sur les convocations.
\setlength{\largeurLogo}{4cm}

%% Dimension d'une case du tableau d'emargement
\newlength{\hauteurCaseSignature}
\setlength{\hauteurCaseSignature}{3cm}

\newlength{\largeurCaseSignature}
\setlength{\largeurCaseSignature}{5cm}


%%
%% Gestion des dates
%%
\newdateformat{dateCompleteFormat}{\dayofweekname{\THEDAY}{\THEMONTH}{\THEYEAR}\ \THEDAY\ \monthname[\THEMONTH]\ \THEYEAR}
\newdateformat{dateCourteFormat}{\THEDAY\ \monthname[\THEMONTH]\ \THEYEAR}
\renewcommand{\timeseparator}{h}

\newcommand{\dateComplete}[2]{\dateCompleteFormat\formatdate{#1}{#2}{\lAnneeDEpreuveVariable}}
\newcommand{\dateCourte}[2]{\dateCourteFormat\formatdate{#1}{#2}{\lAnneeDEpreuveVariable}}
\newcommand{\dateHeure}{\dateCourteFormat\today\ à \currenttime}




%%
%%    Préparation des bases de données
%%

% BDD des étudiants
\DTLnewdb{etudiantsBDD}
\DTLaddcolumn{etudiantsBDD}{Nom}
\DTLaddcolumn{etudiantsBDD}{Prenom}
\DTLaddcolumn{etudiantsBDD}{Genre}
\DTLaddcolumn{etudiantsBDD}{Jour}
\DTLaddcolumn{etudiantsBDD}{Mois}
\DTLaddcolumn{etudiantsBDD}{Heure}
\DTLaddcolumn{etudiantsBDD}{Minute}
\DTLaddcolumn{etudiantsBDD}{Salle}
\DTLaddcolumn{etudiantsBDD}{Loge}
\DTLaddcolumn{etudiantsBDD}{TiersTemps}

% BDD des groupes
\DTLnewdb{groupesBDD}
\DTLaddcolumn{groupesBDD}{Hash}
\DTLaddcolumn{groupesBDD}{Jour}
\DTLaddcolumn{groupesBDD}{Mois}
\DTLaddcolumn{groupesBDD}{Heure}
\DTLaddcolumn{groupesBDD}{Minute}
\DTLaddcolumn{groupesBDD}{Salle}
\DTLaddcolumn{groupesBDD}{Loge}
\DTLgetcolumnindex{\grpHashIndex}{groupesBDD}{Hash}



%%
%%    ajout d'un seul etudiant
%%
\newcommand{\ajoutEtudiant}[8]{
\edef\edate{#4}%
\edef\eheure{#5}%
\edef\esalle{#6}%
\edef\inloge{#7}%
\edef\intt{#8}%
% pré-traitement date
\expandafter\DTLsplitstring\expandafter{\edate}{/}{\jj}{\mm}%
% pré-traitement heure
\expandafter\DTLsplitstring\expandafter{\eheure}{h}{\hh}{\dd}%
\def\tmpvide{}
\ifx\tmpvide\dd
\def\dd{00}
\fi
% pré-traitement loge
\def\tmploge{LOGE}
\ifx\tmploge\inloge
\def\eloge{OUI}
\else
\def\eloge{NON}
\fi
% pré-traitement tiers temps
\def\tmptt{TT}
\ifx\tmptt\intt
\def\ett{OUI}
\else
\def\ett{NON}
\fi
% intégration des données dans la base étudiants
\DTLnewrow{etudiantsBDD}%
\dtlexpandnewvalue%
\DTLnewdbentry{etudiantsBDD}{Nom}{#1}%
\DTLnewdbentry{etudiantsBDD}{Prenom}{#2}%
\DTLnewdbentry{etudiantsBDD}{Genre}{#3}%
\DTLnewdbentry{etudiantsBDD}{Jour}{\jj}%
\DTLnewdbentry{etudiantsBDD}{Mois}{\mm}%
\DTLnewdbentry{etudiantsBDD}{Heure}{\hh}%
\DTLnewdbentry{etudiantsBDD}{Minute}{\dd}%
\DTLnewdbentry{etudiantsBDD}{Salle}{\esalle}%
\DTLnewdbentry{etudiantsBDD}{Loge}{\eloge}%
\DTLnewdbentry{etudiantsBDD}{TiersTemps}{\ett}%
% intégration des données dans la base groupe
\edef\leHash{\twodigit{\mm}\twodigit{\jj}-\twodigit{\hh}\twodigit{\dd}-\esalle}%
\xdtlgetrowindex{\rowcs}{groupesBDD}{\grpHashIndex}{\leHash}%
\ifx\rowcs\dtlnovalue%
   \DTLnewrow{groupesBDD}%
   {%% le groupe n'existe pas. On le crée
     \dtlexpandnewvalue%
     \DTLnewdbentry{groupesBDD}{Hash}{\leHash}%
     \DTLnewdbentry{groupesBDD}{Jour}{\jj}%
     \DTLnewdbentry{groupesBDD}{Mois}{\mm}%
     \DTLnewdbentry{groupesBDD}{Heure}{\hh}%
     \DTLnewdbentry{groupesBDD}{Minute}{\dd}%
     \DTLnewdbentry{groupesBDD}{Salle}{\esalle}%
     \DTLnewdbentry{groupesBDD}{Loge}{\eloge}%
   }%
\fi%
}



%%
%%    ajout d'un fichier CSV
%%
\newcommand{\ajoutFichierCSV}[1]{%
\DTLloaddb{fileBDD}{#1}%
%% On remplit la nouvelle base
\DTLforeach*{fileBDD}{\leNom =Nom, \lePrenom =Prenom, \leGenre =Genre, \laDate =Date, \lHeure =Heure, \laSalle =Salle, \laLoge =Loge, \leTiersTemps =TiersTemps}{%
  \ajoutEtudiant{\leNom}{\lePrenom}{\leGenre}{\laDate}{\lHeure}{\laSalle}{\laLoge}{\leTiersTemps}%
}%
}





%%
%% Cette partie permet de contrôler les éléments communs à toutes les lettres de convocation
%%
\newcommand{\leBTSLongVariable}{Nom Long et Complet}
\newcommand{\leBTSLong}[1]{\renewcommand{\leBTSLongVariable}{#1}}

\newcommand{\leBTSCourtVariable}{NLC}
\newcommand{\leBTSCourt}[1]{\renewcommand{\leBTSCourtVariable}{#1}}

\newcommand{\lAnneeEtudiantVariable}{1\up{ère}}

\newcommand{\MatiereVariable}{Mathématiques}
\newcommand{\deMatiereVariable}{de }
\newcommand{\laMatiereCCF}[1]{%
  \renewcommand{\MatiereVariable}{#1}%
  \StrChar{#1}{1}[\FirstChar]%
  \expandafter\DTLifinlist\expandafter{\FirstChar}{a,e,i,o,u,y,A,E,I,O,U,Y,À,Â,Ä,É,È,Ê,Ë}%
  {\renewcommand{\deMatiereVariable}{d'}}{\renewcommand{\deMatiereVariable}{de }}%
}

\newcommand{\lAnneeDEtudeVariable}{1}
\newcommand{\lAnneeDEtude}[1]{\renewcommand{\lAnneeDEtudeVariable}{#1}%
\renewcommand{\lAnneeEtudiantVariable}{\lAnneeDEtudeVariable\DTLifstringeq*{\lAnneeDEtudeVariable}{1}{\up{ère}}{\up{ème}} année}%
}

\newcommand{\nomProviseurVariable}{John Hammond}
\newcommand{\leNomProviseur}[1]{\renewcommand{\nomProviseurVariable}{#1}}

\newcommand{\qualiteProviseurVariable}{Le proviseur}

\newcommand{\genreProviseurVariable}{M}   %% M ou F
\newcommand{\leGenreProviseur}[1]{\renewcommand{\genreProviseurVariable}{#1}%
\renewcommand{\qualiteProviseurVariable}{L\DTLifstringeq*{\genreProviseurVariable}{M}{e}{a} proviseur\DTLifstringeq*{\genreProviseurVariable}{F}{e}{}}%
}

\newcommand{\logoLyceeVariable}{}
\newcommand{\leLogoLycee}[1]{\renewcommand{\logoLyceeVariable}{#1}}

\newcommand{\adresseCompleteLyceeVariable}{Lycée privé InGen\\ 32, Quai du T-Rex\\0000 Isla Nublar}

\newcommand{\nomCompletLyceeVariable}{Lycée privé InGen}
\newcommand{\leNomCompletLycee}[1]{\renewcommand{\nomCompletLyceeVariable}{#1}%
\renewcommand{\adresseCompleteLyceeVariable}{\nomCompletLyceeVariable\\ \adresseLyceeVariable}%
}

\newcommand{\adresseLyceeVariable}{32, Quai du T-Rex\\0000 Isla Nublar}
\newcommand{\lAdresseLycee}[1]{\renewcommand{\adresseLyceeVariable}{#1}%
\renewcommand{\adresseCompleteLyceeVariable}{\nomCompletLyceeVariable\\ \adresseLyceeVariable}%
}

\newcommand{\villeVariable}{Isla Nublar}
\newcommand{\laVilleLycee}[1]{\renewcommand{\villeVariable}{#1}}

\newcommand{\telephoneLyceeVariable}{}
\newcommand{\leTelephoneLycee}[1]{\renewcommand{\telephoneLyceeVariable}{#1}}

\newcommand{\laDateCourrierVariable}{\dateCourteFormat\today}
\newcommand{\laDateCourrier}[2]{\renewcommand{\StrCut{#1}{/}{\jj}{\mm}\laDateCourrierVariable}{\dateCourte{\jj}{\mm}}}

\newcommand{\lAnneeDEpreuveVariable}{\the\year}
\newcommand{\lAnneeDEpreuve}[1]{\renewcommand{\lAnneeDEpreuveVariable}{#1}}





%%
%%
%%  Page TODO pour ne rien oublier
%%
%%
\newenvironment{TodoListe}{
\begin{center}{\Huge \textbf{Pense-bête CCF}}\\
\bigskip\textbf{\huge BTS \leBTSCourtVariable\lAnneeDEtudeVariable\ \lAnneeDEpreuveVariable}\end{center}

\vspace{\stretch{2}}

\begin{itemize}
    \renewcommand{\labelitemi}{$\square$}
    \setlength{\itemsep}{\stretch{1}}
}
{\end{itemize}

\vspace{\stretch{4}}

\begin{center} {\tiny Ces documents ont été générés par un script \LaTeX\ de Vincent Deveaux.
    Pensez à le remercier, je le connais, ça lui fera plaisir !} \end{center}

\clearpage
}






%%
%%
%%  Convocations
%%
%%
\newcommand{\Convocations}{
\DTLsort{Nom,Prenom}{etudiantsBDD}%
\DTLforeach*{etudiantsBDD}{\leNom =Nom, \lePrenom =Prenom, \leGenre =Genre, \leJour =Jour, \leMois =Mois, \lHeure =Heure, \laMinute =Minute, \laSalle =Salle, \laLoge =Loge}{
 
  \begin{letter}{À \textbf{\lePrenom\ \leNom}\\Étudiant\DTLifstringeq*{\leGenre}{F}{e}{} en BTS \leBTSCourtVariable, \lAnneeEtudiantVariable\\ \nomCompletLyceeVariable}

    \date{le \laDateCourrierVariable}
    \name{\qualiteProviseurVariable}
    \signature{\qualiteProviseurVariable,\\\nomProviseurVariable}
    \lieu{\villeVariable}
    \address{\DTLifstringeq*{\logoLyceeVariable}{}{\nomCompletLyceeVariable}{\includegraphics[width=\largeurLogo]{\logoLyceeVariable}}\\ \adresseLyceeVariable}
    \DTLifstringeq*{\telephoneLyceeVariable}{}{\telephone{}}{\telephone{\telephoneLyceeVariable}}
    \fax{}

    \def\concname{Objet : ~}
    \conc{Convocation au CCF \deMatiereVariable\MatiereVariable\ du Brevet de Technicien Supérieur «\leBTSLongVariable»}
    \opening{\DTLifstringeq*{\leGenre}{F}{Madame}{Monsieur},}

    Vous êtes convoqué\DTLifstringeq*{\leGenre}{F}{e}{} au Contrôle en Cours de Formation \deMatiereVariable
    «\MatiereVariable» du Brevet de Technicien Supérieur «\leBTSLongVariable» qui se déroulera au :

    \begin{center}
      \begin{minipage}{7cm}
        \adresseCompleteLyceeVariable
      \end{minipage}
    \end{center}

    \textbf{Le \dateComplete{\leJour}{\leMois}, salle \laSalle\ à partir de \lHeure h \laMinute.}

    \DTLifstringeq*{\laLoge}{OUI}{\textit{Par ailleurs, veuillez noter que vous serez astreint\DTLifstringeq*{\leGenre}{F}{e}{} à un \textbf{temps de loge d'une heure}
        après votre épreuve}.}{}

    Vous voudrez bien vous munir de la présente convocation en vue du passage de l'épreuve
    ainsi que d'une pièce d'identité en cours de validité.

    Je vous informe que toute absence injustifiée (seuls des motifs graves peuvent être justifiables)
    entraîne une note 0 à ce Contrôle en Cours de Formation.

    \closing{Je vous prie d'agréer, \DTLifstringeq*{\leGenre}{F}{Madame}{Monsieur},
      l'expression de ma considération distinguée.}

  \end{letter}
}
}








%%
%%
%%   Heures de passage pour l'administration
%%
%%
\newcommand{\ListeAdministration}{
\DTLsort{Mois,Jour,Heure,Minute,Salle}{groupesBDD}
\DTLsort{Nom,Prenom}{etudiantsBDD}
\begin{center}
  BTS \leBTSLongVariable,\\
  horaires et date de passage du CCF \lAnneeEtudiantVariable,\\
  Épreuve \deMatiereVariable\MatiereVariable\ \lAnneeDEpreuveVariable.
\end{center}

\vspace{\stretch{1}}

\newcounter{tierstemps}
\setcounter{tierstemps}{0}

\DTLforeach*{groupesBDD}{\leHash =Hash, \laGSalle =Salle, \leGJour =Jour, \leGMois =Mois, \laGHeure =Heure, \laGMinute =Minute, \laGLoge =Loge}%
{
  \begin{center}
      \rowcolors{2}{gray!10}{gray!0}%
      \begin{tabular}{|c|}%
        \hline%
        \rowcolor{gray!50} \bfseries \large \rule{0pt}{3ex} \dateCourte{\leGJour}{\leGMois}, \laGHeure h\laGMinute, salle \laGSalle%
        \DTLifstringeq*{\laGLoge}{OUI}{\\ \rowcolor{gray!50} \scriptsize (Étudiants soumis à une heure de loge après leur épreuve)}{\\ \rowcolor{gray!50} }%
        \DTLforeach*[\DTLiseq{\leGJour}{\leJour} \AND \DTLiseq{\leGMois}{\leMois} \AND \DTLiseq{\laGHeure}{\lHeure} \AND \DTLiseq{\laGMinute}{\laMinute} \AND \DTLiseq{\laGSalle}{\laSalle}]
                    {etudiantsBDD}{\leNom =Nom, \lePrenom =Prenom, \leJour =Jour, \leMois =Mois, \lHeure =Heure, \laMinute =Minute, \laSalle =Salle, \leTiersTemps =TiersTemps}{%
                      \\%
                      \hline%
                      \leNom\ \rule[-1ex]{0pt}{4ex} \lePrenom\ \DTLifstringeq*{\leTiersTemps}{OUI}{(*)\stepcounter{tierstemps}}{}%
                    } \\%
                    \hline
      \end{tabular}%
  \end{center}%
  \vspace{\stretch{1}}
}

\DTLifstringeq*{\arabic{tierstemps}}{0}{Pas de tiers-temps}{{\tiny (*): Étudiant(e) bénéficiant d'un tiers-temps.}}
\clearpage
}



%%
%%
%%   Listes d'émargement
%%
%%
\newcommand{\ListeEmargement}{
\setcounter{page}{1} %% hack pour que la liste d'émargement commence à la page 1.
\setcounter{emargecnt}{1}
\DTLsort{Nom,Prenom}{etudiantsBDD}%
\begin{center}
  BTS \leBTSLongVariable,\\
  Liste d'émargement CCF \lAnneeEtudiantVariable,\\
  Épreuve \deMatiereVariable\MatiereVariable\ \lAnneeDEpreuveVariable.
\end{center}

\newcommand{\haut}{\rule[-\dimexpr\hauteurCaseSignature/2]{0pt}{\hauteurCaseSignature}}

\tablefirsthead{\hline \multirow{2}{*}{Nom Prénom} & Réception de convocation & Passation CCF \\ & {\tiny Date et signature} & {\tiny Date et signature}\\ \hline}
\tablehead{\hline \multirow{2}{*}{Nom Prénom\stepcounter{emargecnt}} & Réception de convocation & Passation CCF \\ & {\tiny Date et signature} & {\tiny Date et signature}\\ \hline}

\tabletail{\hline \multicolumn{3}{c}{BTS \leBTSCourtVariable, \lAnneeEtudiantVariable. Liste éditée le \dateHeure.\hfill page \thepage/\ref{emargetotal}}\\}
\tablelasttail{\hline \multicolumn{3}{c}{BTS \leBTSCourtVariable, \lAnneeEtudiantVariable. Liste éditée le \dateHeure.\hfill page \thepage/\ref{emargetotal}}\\}


\begin{center}
  \begin{supertabular}{|c|>{\centering\arraybackslash}p{\largeurCaseSignature}|>{\centering\arraybackslash}p{\largeurCaseSignature}|}%
    \DTLforeach*{etudiantsBDD}{\leNom =Nom, \lePrenom =Prenom}{%
      \DTLiffirstrow{}{\\ \hline}% %% hack pour éviter un saut de ligne intempestif en haut des données du tableau.
      \leNom\ \lePrenom & \haut & \haut%
    } \\%
  \end{supertabular}
\end{center}

\makeatletter
\protected@write\@auxout{}{\string\newlabel{emargetotal}{\theemargecnt}}
\makeatother
\clearpage
}




%%
%%
%% Pour debug
%%
%%
% Affiche les bases de données.
\newcommand{\printEtudiants}{\DTLdisplaydb{etudiantsBDD}}
\newcommand{\printGroupes}{\DTLdisplaydb{groupesBDD}}


%%
%%
%%    FIN de la classe.
%%
%%


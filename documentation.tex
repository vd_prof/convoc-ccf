%%
%% Ceci est la documentation de ma classe pour éditer mes convocations pour les CCF de mes BTS.
%%
%% V. Deveaux (c) 2024
%% Pour toute question, vincent.deveaux@ac-grenoble.fr
%%

%% DISCLAIMER:
%%
%% Cette classe est fournie sans prétention aucune. Elle n'est pas parfaite.
%% Je ne garantie pas qu'il n'y ait aucun bug ou problème de compatibilité.
%% Je décline toute responsabilité en cas de perte de données ou autre, 
%% supposé être causé par l'utilisation de cette classe.
%%

%% LICENCE:
%%
%% Je place ce document, ainsi que la classe TeX et le document d'exemple
%% sous licence WTFPL.
%%
%% Voir www.wtfpl.net pour plus de précision (ou la page wikipedia de la WTFPL)

\documentclass[a4paper,11pt]{article}
\usepackage{a4wide}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[french]{babel}
\usepackage[tikz]{mdframed}

\newcommand{\chaine}[1]{\colorbox{yellow!15}{\texttt{#1}}}
\newcommand{\cmd}[1]{\chaine{\textbackslash #1}}
\newcommand{\cmdarg}[2]{\cmd{#1\{\textit{<#2>}\}}}


\newenvironment{boitegrise}{\begin{mdframed}[backgroundcolor=gray!20]\ttfamily}{\end{mdframed}}
\newenvironment{boitejaune}{\begin{mdframed}[backgroundcolor=yellow!15]\ttfamily}{\end{mdframed}}



\title{Convocations}
\author{V. Deveaux}
\date{8 mai 2024}

\begin{document}

\maketitle

Ceci est la version 0.99 de ma classe \LaTeX\ pour générer les convocations de CCF de mathématiques pour les BTS.
Elle peut aussi générer les listes d'émargement et les listes pour l'administration.

\bigskip

\noindent
\emph{Convention}: Dans ce document, une commande ou une chaîne de caractères est exprimée en caractères
type machine à écrire: \cmd{commande}\ ou \chaine{chaine}.
Pour les arguments des commandes, elles sont entre chevrons et en italique: \chaine{\textit{<arg>}}.

\bigskip

Pour utiliser cette classe, il faut l'indiquer dans le \chaine{documentclass}:

\begin{boitejaune}
\textbackslash documentclass\{convocations\}
\end{boitejaune}

\medskip\noindent
Cette classe n'a pas d'option.

%%
%%
%%
%%
\section{Personnalisation des courriers}

Voici les commandes qui permettent la personnalisation des convocations.
Par défaut, elles ont des valeurs aberrantes.
\begin{description}
\item[\cmdarg{leBTSLong}{nom}]: Nom long et complet du BTS.

\item[\cmdarg{leBTSCourt}{nom}]: Nom court. Souvent l'acronyme du nom du BTS.

\item[\cmdarg{lAnneeDEtude}{nb}]: L'année d'étude. \chaine{1} ou \chaine{2}.

\item[\cmdarg{laMatiereCCF}{nom}]: La matière sur laquelle porte l'épreuve.

\item[\cmdarg{leNomProviseur}{nom}]: Nom du proviseur du lycée de convocation.

\item[\cmdarg{leGenreProviseur}{genre}]: Genre du proviseur: \chaine{M} ou \chaine{F}.
	Pour accord dans les lettres de convocation.

\item[\cmdarg{leLogoLycee}{fic}]: Nom du fichier qui contient le logo du lycée. Format png ou jpeg.
	Il est possible de ne pas avoir de logo.\\
	Pour la taille de l'image, voir la longueur \cmd{largeurLogo}.

\item[\cmdarg{leNomCompletLycee}{nom}]: Nom complet du lycée de convocation.

\item[\cmdarg{lAdresseLycee}{adresse}]: Adresse du lycée qui convoque.
Il ne doit pas y avoir son nom, juste l'adresse.
	Utiliser \chaine{\textbackslash\textbackslash} pour sauter une ligne.

\item[\cmdarg{laVilleLycee}{ville}]: Nom de la ville du lycée de convocation.

\item[\cmdarg{leTelephoneLycee}{tel}]: Numéro de téléphone du lycée qui convoque.
	L'argument est vide par défaut.

\item[\cmdarg{laDateCourrier}{date}]: Date des courriers de convocation.
	Par défaut, c'est le jour de compilation. (format obligatoire: \chaine{31/2} ou \chaine{31/02}).
	Cette date est différente de la date de convocation.

\item[\cmdarg{lAnneeDEpreuve}{annee}]: Année de l'épreuve. Par défaut, l'année actuelle (format: \chaine{2004}).
\end{description}




%%
%%
%%
%%
\section{Les données}

Il y a deux façons d'ajouter des données: soit par un fichier CSV, soit en ajoutant les étudiants 
un par un directement dans le fichier TeX.
Les deux méthodes ne sont pas exclusives.
Malgré tout, je déconseille fortement de mixer les deux:
c'est le meilleur moyen de ne pas retrouver ses données et de tout mélanger.

Les étudiants n'ont pas nécessairement besoin d'être entrés dans l'ordre alphabétique:
ils seront triés avant la création des convocations.


%%
%%
\subsection{Dans un fichier CSV}

La commande d'ajout de fichier CSV est \cmdarg{ajoutFichierCSV}{fic}

Un fichier CSV est un simple fichier texte. C'est donc facile à écrire ou à lire par un humain.
Par contre, la structure est assez rigide.

La première ligne doit être:

\begin{boitegrise}
Nom,Prenom,Genre,Date,Heure,Salle,Loge,TiersTemps
\end{boitegrise}

\medskip\noindent
L'ordre des données peut changer, par contre il ne faut pas d'espace ni d'accent 
et bien respecter les minuscules/majuscules !
C'est important sinon, \LaTeX\ ne trouvera pas les données et la compilation va planter...

Les lignes suivantes doivent contenir les informations des étudiants, une ligne par étudiant.
À l'intérieur d'une ligne, les données sont organisées dans l'ordre de la première ligne.
Elle sont séparées par une virgule, toujours sans espace !

Attention: pas de ligne vide !

\bigskip

Quelques précisions:
\begin{description}
\item[Nom, Prenom]: c'est clair. Ici, les accents, minuscules et majuscules sont acceptés.
Pour les noms et/ou prénoms composés, on peut utiliser des espaces entre les mots.

\item[Genre]: Genre de l'étudiant: \chaine{M} ou \chaine{F}.
	Pour accord dans les lettres de convocation.

\item[Date, Heure, Salle]: date, heure et salle de passage du CCF.
	Le format de la date est obligatoirement j/m ou jj/mm (par exemple \chaine{24/4} ou \chaine{24/04})
	et le format de l'heure est \chaine{8h05}, \chaine{08h05} ou \chaine{9h}.
	La salle est une chaîne de caractères.

\item[Loge]: Si l'étudiant doit rester en loge après son CCF. \chaine{LOGE} si loge, une autre
	chaîne sinon.
	Explication: Pour ma part, je sépare les étudiants en trois groupes que je fais passer
	à la suite l'un de l'autre.
	Pour éviter qu'ils ne communiquent à propos du sujet,
	le premier groupe est soumis à une heure de loge après son épreuve, le temps que le deuxième groupe compose.
	Pendant cette heure, ils sont surveillés par un surveillant (AED ou collègue) dans une salle à coté
	et n'ont pas le droit de communiquer avec l'extérieur.
	Une fois que le deuxième groupe a fini, je libère tout le monde et fait passer le troisième groupe.
	Cette heure de loge est indiquée sur la convocation des étudiants concernés.

\item[TiersTemps]: L'étudiant bénéficie-t-il d'un tiers-temps ? \chaine{TT} si tiers temps, une autre
	chaîne sinon.
	Cette information est destinée à l'administration. Elle n'est pas indiquée sur les convocations.
\end{description}


%%
%%
\subsection{Dans le fichier TeX}

Pour l'ajout dans le fichier TeX, la commande est \cmd{ajoutEtudiant}.
Cette commande nécessite 8 arguments dans cet ordre:\\
\chaine{\{\textit{<nom>}\}\{\textit{<prenom>}\}\{\textit{<genre>}\}\{\textit{<date>}\}\{\textit{<heure>}\}\{\textit{<salle>}\}\{\textit{<loge>}\}\{\textit{<tierstemps>}\}}.

Les champs sont les même que ceux du fichier CSV. Reportez-vous à la section correspondante pour plus d'info.



%%
%%
%%
%%
\section{Longueurs}

Quelques longueurs peuvent être modifiées (Valeur par défaut entre parenthèses):
\begin{description}
\item[\cmd{largeurLogo}]: (4cm). C'est la largeur de l'image qui sert de logo au lycée.
	La hauteur se calcule toute seule pour garder les proportions de l'image intactes.
	Cette longueur n'est utile que si on a fourni un fichier d'image. Voir \cmd{leLogoLycee}.
\item[\cmd{hauteurCaseSignature}]: (3cm). C'est la hauteur d'une case de signature d'un étudiant
	dans la liste d’émargement.
\item[\cmd{largeurCaseSignature}]: (5cm). C'est la largeur d'une case de signature d'un étudiant
	dans la liste d’émargement.
\end{description}

La plupart du temps, on cherche à modifier la taille des cases de signature car la dernière page
de la liste d’émargement ne comporte aucun, un seul ou deux nom(s) et c'est disgracieux.



%%
%%
%%
%%
\section{Commandes de production}

Voici les principales commandes, celles qui font véritablement quelque chose:
\begin{description}
\item[\cmd{Convocations}]: Permet de créer les lettres de convocation des étudiants.
	Une page par étudiant, dans l'ordre alphabétique des noms de famille.

\item[\cmd{ListeAdministration}]: Permet de créer une liste récapitulative.
	Elle regroupe les étudiants par groupe, en précisant les dates, heures et salles de passage ainsi que les
	éventuels tiers-temps et temps de loge.
	Cette liste est destinée à l'administration.

\item[\cmd{ListeEmargement}]: Permet de créer la liste d'émargement.
	Les étudiants auront deux signatures à apposer: une à la réception de la convocation, une autre lors du CCF.
	Je demande qu'ils indiquent aussi les dates à chaque fois.

	Le fichier doit être compilé deux fois de suite pour prendre en compte le nombre de pages correctement.
\end{description}


Il existe aussi un environnement \texttt{TodoListe} qui permet de créer une TODO liste des choses à faire
pour préparer le CCF. Cette liste est destinée au prof qui organise. Pour ne rien oublier.
Fondamentalement, c'est un itemize; il faut donc une liste d'\cmd{item} dans son corps.
Par défaut, elle ne contient rien, c'est à vous de la remplir...


%%
%%
%%
%%
\section{Auteur et disclamer}

Cette classe a été écrite par Vincent Deveaux, \texttt{vincent.deveaux[AT]ac-grenoble.fr}.

\medskip
Elle me sert tous les ans. Elle est fournie sans aucune prétention.
Si elle vous est utile aussi, je suis content.

\bigskip

Si vous trouvez un bug, si vous avez un soucis avec cette classe ou si sous avez une idée pour l'améliorer,
n'hésitez pas à me contacter. Je ne vous promets pas de corriger le problème ou d'implémenter votre idée
mais je vous promets d'y jeter un coup d'oeil.



%%
%%
%%
%%
\section{Licence}

La classe \chaine{convocations}, sa documentation et son exemple sont placés
sous licence WTFPL dont voici le texte intégral:

\begin{boitegrise}
\ttfamily
\begin{center}
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\\
Version 2, December 2004
\end{center}

\medskip
Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

\medskip
Everyone is permitted to copy and distribute verbatim or modified\\
copies of this license document, and changing it is allowed as long\\
as the name is changed.

\begin{center}
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\\
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
\end{center}

0. You just DO WHAT THE FUCK YOU WANT TO.
\end{boitegrise}

\end{document}

